Athan - “¿Así es como termina mi historia?”  Claro que no, aún estoy empezando 






















Demigod Revenger

Créditos


Desarrollado por:

	Alejandro Pinedo Barrios

	Abelardo Valdez Poot

	Gustavo Adolfo Ramírez Casillas

	Martín Moisés Márquez Cárdenas




Creadores de Historia:

	Alejandro Pinedo Barrios

	Abelardo Valdez Poot

	Gustavo Adolfo Ramírez Casillas

	Martín Moisés Márquez Cárdenas



Diseñadores de personajes:

	Gustavo Adolfo Ramírez Casillas

	Alejandro Pinedo Barrios



Diseño de Página

	Martín Moisés Márquez Cárdenas

	Abelardo Valdez Poot



Documentación

	Alejandro Pinedo Barrios

	Abelardo Valdez Poot

	Gustavo Adolfo Ramírez Casillas

	Martín Moisés Márquez Cárdenas


Agradecimientos a:

	Carlos Erick Galván Tejada




Motor: RPG Maker MV

	Kadokawa/Enterbrain




Imagenes utilizadas

	Johannes Plenio 
	lextotan


	

Canción de los creditos

	Audio Library – Música para creadores de contenido



Emotional Faces:

	Palxan and Verdibona


Animations Collection III - Thaumaturgy

	[Copyright](C) Degica, Co. LTD
	Resource creators: Andy Chen


RPG Maker MV - Essentials Set

	[Copyright](C) Enterbrain
	Resource creators: Murray Atkinson, Michael Rookard, Archeia, Lunarea



Add-on Series

	[Copyright](C) KADOKAWA CORPORATION./YOJI OJIMA

Sangokushi Graphic Pack

	(C) 2017 KADOKAWA CORPORATION./YOJI OJIMA






De parte del equipo creador de DEMIGOD REVENGER
Gracias por jugar 






DEMIGOD REVENGER